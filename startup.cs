using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

public class Startup
{
    public void Configure(IApplicationBuilder app)
    {
        app.UseStaticFiles();
        app.UseWebSockets();
        app.UseSignalR();


        // app.UseMvc(routes =>
        // {
        //     routes.MapRoute(
        //         name: "default",
        //         template: "api/{controller}/{action}/{id?}"
        //     );
        // });

        // //ADDING AND HANDLING CUSTOM ROUTES
        // var routeBuilder = new RouteBuilder(app);

        // routeBuilder.MapGet("", context => 
        //                 context.Response.WriteAsync("Hello from root!"));

        // routeBuilder.MapGet("hello", context => 
        //                 context.Response.WriteAsync("Hello from /hello"));

        // routeBuilder.MapGet("hello/{name}", context => 
        //                 context.Response.WriteAsync($"Hello, {context.GetRouteValue("name")}"));

        // routeBuilder.MapGet("square/{number:int}", context =>
        // {
        //     int number = Convert.ToInt32(context.GetRouteValue("number"));
        //     return context.Response.WriteAsync($"The square of {number} is {number * number}");
        // });

        // routeBuilder.MapPost("post", context => context.Response.WriteAsync("Posting!"));

        // app.UseRouter(routeBuilder.Build());
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSignalR(options => 
        {
            options.Hubs.EnableDetailedErrors = true;
        });

        //services.AddRouting();
        // services.AddMvc();
        // services.AddSingleton<IPostRepository, PostRepository>();
    }
}